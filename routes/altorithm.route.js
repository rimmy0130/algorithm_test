const router = require('express').Router();


router.post('/test001', (req, res) => {
    let samples = req.body;

    const solution = (str) => {
        var answer = false;

        // logic
        if(!isNaN(parseInt(str))){
            if(str.length === 4 || str.length === 6){
                answer = true;
            }
        }
        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.input);
        return testcase;
    });

    res.send(result);
});

router.post('/test002', (req, res) => {
    let samples = req.body;

    const solution = (str) => {
        var answer = [];
        var str = str + "";
        // logic
        var arr = str.split("");
        arr.reverse();
        for(var i=0; i<arr.length; i++){
            answer.push(parseInt(arr[i]));
        }
        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.input);
        return testcase;
    });

    res.send(result);
});

router.post('/test003', (req, res) => {
    let samples = req.body;

    const solution = (m, tree) => {
        var answer = "";
        var leftIdx = 0;
        var rightIdx = tree.length - 1;
        // logic
        while(leftIdx <= rightIdx){
            var midIdx = Math.floor((leftIdx + rightIdx) / 2);
            var pivot = Math.floor((tree[leftIdx] + tree[rightIdx]) / 2);
            var sum = 0;

            for(var idx=0; idx<tree.length; idx++){
                if(tree[idx] > pivot){
                    sum += tree[idx]-pivot ;
                }
            }

            if(sum < m){
                rightIdx = midIdx;
            }else if(sum > m){
                leftIdx = midIdx + 1;
            }else{
                answer = pivot;
                break;
            }
        }
        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.m, testcase.tree.sort());
        return testcase;
    });

    res.send(result);
});

router.post('/test004', (req, res) => {
    let samples = req.body;

    const solution = (n, budget, m) => {
        var answer = 0;
        var left = 0;
        var right = m;
        var pivot = Math.floor(m / n);

        // logic
        while(left < right){
            var total = 0;
            for(var i=0; i<budget.length; i++){
                if(budget[i] > pivot){
                    total += pivot;
                }else{
                    total += budget[i];
                }
            }
            
            if(total === pivot*n){
                answer = pivot;
                return answer;
            }

            if(total > m){
                right = pivot -1;
            }else if(total < m){
                left = pivot ;
            }else{
                answer = pivot ;
                return answer;
            }
            answer = pivot -1 ;

            pivot = Math.floor((left + right) / 2);
        }

        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.n, testcase.budget, testcase.m);
        return testcase;
    });

    res.send(result);
});

router.post('/test005', (req, res) => {
    let samples = req.body;

    const solution = (n, times) => {
        var answer = 0;
        var minTime = 0;
        var maxTime = times[times.length-1] * n ;
        // logic
        while(minTime <= maxTime){
            var midTime = Math.floor((minTime + maxTime)/2);
            
            var calN = 0;
            for(var i=0; i<times.length; i++){
                calN += Math.floor(midTime / times[i]);
            }

            if(n < calN){
                maxTime = midTime -1;
            }else if(n > calN){
                minTime = midTime + 1;
            }else if(n === calN){
                break;
            }

        }
        answer = midTime;
        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.n, testcase.times);
        return testcase;
    });

    res.send(result);
});

module.exports = router;